# KINTEL



## Getting started

Clone this repository by using git-bash. Move to the directory of your choice and enter the command  
git clone https://gitlab.com/fhgr/kintel.git

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/fhgr/kintel/-/settings/integrations)

# Einführung in Künstliche Intelligenz und *Machine Learning* (FH Graubünden)

Autoren: Robin Derungs, Lukas Toggenburger, Udo Birk

## Beschreibung

Eine Sammlung von Jupyter Notebooks zur Erarbeitung von Wissen im Umgang mit *Machine Learning*.  
Hier geht es zu den Notebooks:
[Index](./index.ipynb)

## Installationshinweise

[Installation Guide](./KINTEL-InstallationGuide.pdf)

## Literatur und Support

Standardwerk: 
A. Müller und S. Guido: Introduction to machine learning with Python : a guide for data scientists  
Kostenlose Jupyter Notebooks verfügbar unter:  
https://github.com/amueller/introduction_to_ml_with_python

A. Géron: Hands-On Machine Learning with Scikit-Learn & TensorFlow  
Kostenlose Jupyter Notebooks verfügbar unter:  
https://github.com/ageron/handson-ml2 

J. VanderPlas: Python Data Science Handbook  
Kostenlose Jupyter Notebooks verfügbar unter:  
https://github.com/jakevdp/PythonDataScienceHandbook
